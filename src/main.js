import addJoinOurProgramSection from './join-us-section.js';
import updateCommunityCards from './community.js';
import './styles/style.css';

document.addEventListener('DOMContentLoaded', () => {
    addJoinOurProgramSection('standard');

    // Make a GET request using fetch and call the function
    fetch('/community')
        .then((response) => {
            if (!response.ok) {
                throw new Error(`Request failed with status: ${response.status}`);
            }
            return response.json();
        })
        .then((data) => {
            updateCommunityCards(data);
        })
        .catch((error) => {
            console.error('Request failed:', error);
        });
});
