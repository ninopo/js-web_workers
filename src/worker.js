let dataBatch = [];

self.addEventListener('message', (event) => {
    dataBatch.push(event.data);

    if (dataBatch.length >= 5) {
        sendBatchToServer();
    }
});

async function sendBatchToServer() {
    try {
        const response = await fetch('/analytics/user', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(dataBatch),
        });

        if (!response.ok) {
            throw new Error('Network response was not ok');
        }
        dataBatch = [];
    } catch (error) {
        console.error('Failed to send batch to server:', error);
    }
}